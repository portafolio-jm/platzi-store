export const environment = {
  production: true,
  url_api: 'http://platzi-store.herokuapp.com',
  firebase: {
    apiKey: 'AIzaSyBG73SOzJsg150tnwu3qIkSVNViNFfQDBY',
    authDomain: 'platzi-store-ebb0b.firebaseapp.com',
    databaseURL: 'https://platzi-store-ebb0b.firebaseio.com',
    projectId: 'platzi-store-ebb0b',
    storageBucket: 'platzi-store-ebb0b.appspot.com',
    messagingSenderId: '723092782141',
    appId: '1:723092782141:web:0c5858ea193d1531f17e54'
  }
};
