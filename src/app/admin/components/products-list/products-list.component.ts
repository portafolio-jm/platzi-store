import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../../../core/services/products/products.service';
import { Product } from '../../../product.model';

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.scss']
})
export class ProductsListComponent implements OnInit {
  dataSource: Product[] = [];
  displayedColumns: string[] = ['id', 'title', 'description', 'price', 'actions'];
  constructor(private productService: ProductsService) {}

  ngOnInit() {
    this.fetchProduct();
  }

  fetchProduct() {
    this.productService.getAllProducts().subscribe(products => {
      this.dataSource = products;
    });
  }

  deleteProduct(id: string) {
    this.productService.deleteProduct(id).subscribe(resp => {
      console.log(resp);
      this.fetchProduct();
    });
  }
}
