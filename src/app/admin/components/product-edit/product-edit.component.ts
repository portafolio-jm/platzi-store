import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { MyValidators } from '../../../utils/validators';
import { ProductsService } from '../../../core/services/products/products.service';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-product-edit',
  templateUrl: './product-edit.component.html',
  styleUrls: ['./product-edit.component.scss']
})
export class ProductEditComponent implements OnInit {
  form: FormGroup;
  idProduct: string;
  constructor(
    private formBuilder: FormBuilder,
    private productService: ProductsService,
    private activeRouter: ActivatedRoute,
    private route: Router
  ) {
    this.buildForm();
  }

  ngOnInit() {
    this.activeRouter.params.subscribe((params: Params) => {
      this.idProduct = params.id;
      this.fetchProduct(this.idProduct);
    });
  }

  private buildForm() {
    this.form = this.formBuilder.group({
      title: ['', [Validators.required]],
      price: ['', [Validators.required, MyValidators.isPriceValid]],
      description: ['', [Validators.required]]
    });
  }

  fetchProduct(id: string) {
    this.productService.getProduct(id).subscribe(product => {
      this.form.patchValue(product);
    });
  }

  saveProduct(event: Event) {
    event.preventDefault();
    if (this.form.valid) {
      const product = this.form.value;
      this.productService.updateProduct(this.idProduct, product).subscribe(newProduct => {
        this.route.navigate(['./admin/products']);
      });
    } else {
      console.log('formulario invalido');
    }
  }

  get priceField() {
    return this.form.get('price');
  }
}
