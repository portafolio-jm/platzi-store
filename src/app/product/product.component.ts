import { Component, Input, Output, EventEmitter, OnChanges, SimpleChanges, OnInit, OnDestroy } from '@angular/core';
import { Product } from '../product.model';
import { CartService } from '../core/services/cart.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnChanges, OnInit, OnDestroy {
  @Input() product: Product;
  @Output() productCliked: EventEmitter<string> = new EventEmitter();

  constructor(private cartService: CartService) {
    // console.log('1.- constructor');
  }

  ngOnChanges(changes: SimpleChanges) {
    // console.log('2.- ngOnChanges', changes);
  }

  ngOnInit() {
    // console.log('3.- ngOnInit');
  }

  // ngDoCheck() {
  //   console.log('4.- ngDoCheck');
  // }

  ngOnDestroy() {
    // console.log('5.- ngOnDestroy');
  }

  addCart() {
    console.log('añadir al carrito');
    // this.productCliked.emit(this.product.id);
    this.cartService.addCart(this.product);
  }
}
