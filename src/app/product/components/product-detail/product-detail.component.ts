import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Product } from '../../../product.model';
import { ProductsService } from '../../../core/services/products/products.service';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss']
})
export class ProductDetailComponent implements OnInit {
  product: Product;
  constructor(private route: ActivatedRoute, private productsService: ProductsService) {}

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      const id = params.id;
      this.fetchProduct(id);
    });
  }

  fetchProduct(id: string) {
    this.productsService.getProduct(id).subscribe(product => {
      this.product = product;
    });
  }

  createProduct() {
    const newProduct: Product = {
      id: '10',
      title: 'nuevo producto desde angular',
      image: 'assets/images/banner-1.jpg',
      description: 'descripción de nuevo producto',
      price: 8000
    };

    this.productsService.createProduct(newProduct).subscribe(product => {
      console.log(product);
    });
  }

  updateProduct(id: string) {
    const updateProduct: Partial<Product> = {
      description: 'descripción Editada',
      price: 10000
    };

    this.productsService.updateProduct(id, updateProduct).subscribe(product => {
      console.log(product);
    });
  }

  deleteProduct(id: string) {
    this.productsService.deleteProduct(id).subscribe(product => {
      console.log(product);
    });
  }
}
